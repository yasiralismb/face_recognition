let monitoring

/**
 * Function to request the server to start the motor
 */
async function requestStartMotor () {
  try {
    start_flag = true
    while(start_flag)
    {
    // Make request to server
    await axios.post('/start_motor')

    // Update status
    updateStatus('Working')
    startMonitoring()            //later have to uncomment
    }
  } catch (e) {
    console.log('Error starting the motor', e)
    updateStatus('Error starting')
  }
}
// stop system
async function requestStopsystem () {
  try {
    
    // Make request to server
    await axios.post('/stop_system')
    start_flag = false
    // Update status
    updateStatus('stopped')
    startMonitoring()            //later have to uncomment
  } catch (e) {
    console.log('Error starting the motor', e)
    updateStatus('Error starting')
  }
}

/** 
 * (Re)start monitoring
 */
async function startMonitoring () {
  monitoring = true
  while (monitoring) {
    let result = await axios.get('/monitor')
    updateMonitoringData(result.data)
  }
}

/** 
 * Stop monitoring
 */
function stopMonitoring () {
  monitoring = false
}

/**
 * Function to request monitoring data to the server and display it in the main page
 */
function updateMonitoringData (data) {
  // Get HTML elements where results are displayed
  // Get the HTML element where the status is displayed
  const results = JSON.stringify(data)
  results1 = JSON.parse(results)
  let Monitoring_Data1 = document.getElementById('distance_text')
  Monitoring_Data1.innerText = results1.distance
  let Monitoring_Data2 = document.getElementById('Inzone_text')
  Monitoring_Data2.innerText = results1.inZone
}

/**
 * Function to request the server to stop the motor
 */
function stopSystem () {
  start_flag = false
  updateStatus('The motor and sensor monitoring will stop')
}


function updateStatus(statusText) {
  // Get the HTML element where the status is displayed
  let motor_status_text = document.getElementById('status_text')
  motor_status_text.innerText = statusText
}
