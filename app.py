from flask import Flask, render_template, Response
from camera import VideoCamera
from flask import Flask, jsonify, request
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
@app.route('/')
def index():
    return render_template('index.html')

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/start_motor',methods=['POST'])
def start_motor():
    print('starting motor')
    return { 'success': True }

@app.route('/stop_system',methods=['POST'])
def stop_system():
    print('stopping motor')
    return { 'success': True }

@app.route('/monitor', methods=['GET'])
def monitor():
    #sensor_controller.track_rod()
    distance = 74.444
    zone = "ture"
    return jsonify({
        "inZone" : zone,
        "distance" : distance
        })




if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
    print('program starting')